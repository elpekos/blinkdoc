# Z88 BLINK (NEC D65031GF168)

v1.0 - first version, (c) Thierry PEYCRU, may 2021

## Presentation
The Blink gate-array is a custom ASIC : **NEC D65031GF168**. This SMD is a uPD65000 series (low power 2 micron ASIC) of 3312 cells (138 rows by 24 columns of 4 transistors). GF stands for a flat packaging of 100 pins. 168 is the circuit type number identifying the blink design.

The original notes as supplied to Cambridge Computer were lost during the time of the company's move from Cambridge to Scotland. The following notes comes from the patents WO88/09007 (Richard MILLER and James WESTWOOD) and WO88/09573 (James WESTWOOD). Internal logic has been guessed from chronograms sampled with a logic analyzer.

![](./img/blink_chip.png)
A Blink produced during year 1987, week 14 in factory KK (from Z88 serial #003795, board issue 4).
![](./img/blink_88dk.png)
A later revision, year 1988, week 24, factory KD019 (from Z88 serial #023356, board issue 5).

## Functions
* APM (advanced power management)
* MMU (memory management unit) with shared bus controller
* CPU clock control
* CPU reset
* Keyboard matrix interface
* RTC (real time clock)
* PIC (programmable interrupt controller)
* UART (serial port)
* LCD controller
* EPROM programmmer
* Speaker driver

##Pinout
```
Pin     Name    Type    Description
1       GND     -       Ground
2       VDD     -       +5V
3       IOR     I       Z80 /IORQ
4       CRD     I       Z80 /RD
5       MRQ     I       Z80 /MREQ
6       HLT     I       Z80 /HALT
7       NMIB    O       Z80 /NMI
8       INTB    O       Z80 /INT
9       CDB     IO      Z80 D1
10      ROUT    O       Z80 /RST
11      CDA     IO      Z80 D0
12      CM1     I       Z80 /M1
13      CDH     IO      Z80 D7
14      CDC     IO      Z80 D2
15      CA0     I       Z80 A0
16      CDG     IO      Z80 D6
17      CA1     I       Z80 A1
18      CDF     IO      Z80 D5
19      CA2     I       Z80 A2
20      CDD     IO      Z80 D3
21      CA3     I       Z80 A3
22      CDE     IO      Z80 D4
23      CA4     I       Z80 A4
24      CA5     I       Z80 A5
25      CA15    I       Z80 A15
26      CA6     I       Z80 A6
27      CA14    I       Z80 A14
28      GND     -       Ground
29      VDD     -       +5V

30      CA13    I       Z80 A13
31      CA7     I       Z80 A7
32      CA8     I       Z80 A8
33      CA12    I       Z80 A12
34      CA9     I       Z80 A9
35      CA11    I       Z80 A11
36      CA10    I       Z80 A10
37      TxD     O       UART TXD
38      RTS     O       UART RTS
39      IRCE    O       RAM.0 /CE
40      GND     -       Ground
41      RxD     I       UART RXD
42      CTS     I       UART CTS
43      DCD     I       UART DCD
44      PM1     O       Z80 clock (3.2768 MHz)
45      LP      O       LCD control, 10ms  line pulse
46      FR      O       LCD control, 156us frame
47      XSCL    O       LCD control, 300ns data
48      LD0     O       LCD data
49      LD1     O       LCD data
50      LD2     O       LCD data
51      LD3     O       LCD data

52      VDD     -       +5V
53      GND     -       Ground
54      MA16    O       Memory A16
55      MA15    O       Memory A15
56      MA14    O       Memory A14
57      MA12    O       Memory A12
58      MA7     O       Memory A7
59      MA13    O       Memory A13
60      MA6     O       Memory A6
61      MA8     O       Memory A8
62      MA5     O       Memory A5
63      WRB     O       Write enable /WE
64      MA9     O       Memory A9
65      MA4     O       Memory A4
66      MA11    O       Memory A11
67      MA3     O       Memory A3
68      IPCE    O       ROM.0 /CE
69      MA2     O       Memory A2
70      MA10    O       Memory A10
71      MA1     O       Memory A1
72      MA0     O       Memory A0
73      MDH     IO      Memory D7
74      MDA     IO      Memory D0
75      MDG     IO      Memory D6
76      MDB     IO      Memory D1
77      MDF     IO      Memory D5
78      MDC     IO      Memory D2
79      VDD     -       +5V
80      GND     -       Ground

81      MDE     IO      Memory D4
82      MDD     IO      Memory D3
83      MA17    O       Memory A17
84      MA18    O       Memory A18
85      MAW     O       Memory A19
86      SE1     O       Slot1 /CE
87      POE     O       PSRAM /OE (refreshed)
88      ROE     O       ROM /OE
89      PGMB    O       Slot 3 /WE (Program Byte)
90      EOE     O       Slot 3 /OE (Eprom OE)
91      SE3     O       Slot 3 /CE
92      FLP     I       Flap
93      SE2     O       Slot2 /CE
94      SNS     I       Sens line (card insertion)
95      VPON    O       VPP on
96      BTL     I       Batt low
97      RIN     I       Reset input
98      MCK     I       Master clock (9.8304 MHz)
99      SCK     I       Standby clock (25.6 KHz)
100     SPKR    O       speaker
```

##Power management
The main function of the blink is an advanced power management. HALT input drives blink COMA state. The blink clock is slowed down switching MCK (9.8304MHz) to SCK (25.6KHz). CMOS Z80 is stopped. Inactive circuit area are switched off like UART clocks. External power rails are shut down like RS232 level converters. Moreover, the CMOS Z80 is stopped when waiting for a keypress or a time interrupt (SNOOZE).

Battery level is monitored, BTL is set on low condition (<4.2V). Shutdown is initiated on power failure (<3.2V).

##PM1, Clocks
Z80 clock (PM1) is driven by the Blink, the CMOS Z80 can be stopped without losing register state.

* Active	: PM1 = MCK / 3 = 9.8304 / 3 = 3.2768MHz
* Snooze	: PM1 is stopped
* Coma		: PM1 is stopped, power rails are switched off
* Doze		: PM1 is stopped during selected eprom programming phase (see below)

Master clock is 9.83MHz, three times the CPU and the memory bus. It drives the internal logic feeding the 640bit shift register of the LCD module.

It will be appreciated that the CPU clock must be stopped and started at appropriate points in the clock cycle. In practice it may be stopped halfway through a cycle and restarted at the beginning of another cycle.

##POE, ROE, EOE, pseudo-static RAM refresh
* POE : Pseudo-static OE
* ROE : Rom OE
* EOE : Eprom OE

When Z80 is running and LCD is on, the refresh is performed by the LCD controller reading the memory. When LCD is off (DOZE state), the 42832 is not in standby mode, it needs to be refreshed. POE provides this refresh, ROE does not. POE is tracked to slot 0 RAM, slot 1 and slot 2 connectors. 
Pseudo static RAM card are not supported in slot 3. The POE line in slot 3 connector is replaced by the EOE. EOE is driven by the EPR register.
Z80 Refresh signal is not wired. Refresh logic in blink is done from CRD, CM1, MRQ (negative signals) :

```
POE = ROE | ( CRD & ~CM1 & ~MRQ)
```

##Memory bus controller
Following have been discovered from logic analysis.
Memory bus is interleaved between CPU and screen controller. Memory bus is clocked at 3.2768MHz (305 ns period). During a line pulse of 156.25us, there are 512 clocks (156.25us / 305ns). 108 font bytes have to be read to built one line, 3 clocks per byte (screen base attribute LSB, MSB then screen font). 108 * 3 = 324 clocks are required, 168 available for the CPU. It is more than enough because of Z80 instruction length (4 to 23 T-states).

Typical memory bus layout during snooze :

![](./img/mbus_snz.png)

When Z80 is active, MREQ force CPU access to memory else blink perform screen memory reading.

![](./img/mbus_cpu.png)

Memory bus interleaving below shows one line building in snooze state followed by a tick interrupt. 

![](./img/mbus_int.png)

##LCD controller
The LCD frame rate is 100Hz, one frame every 10ms (FR). A frame is 64 lines. One line is sent every 156.25us. LP is sent when a full row has been shifted to the LCD module. A burst of 160 nibbles at 3.2768 MHz (305ns period) are sent on LD0-LD3 data lines.

Sample below shows 8 SBA1/SBA2/SF cycles after a line pulse. During a cycle, one or two nibbles sent to lcd data lines according lores (6 pixels) or hires (8 pixels) pushed in the internal shift register.

![](./img/xscl_lcd.png)

##SNS, NMI
SNS input is driven by slots edge connectors, expansion edge connector and battery level monitor.

Card insertion or removal is detected by SNS on slot edge connectors. The card module slot connector is made such that during insertion or removal the SNS is shorted to ground. When SNS goes low, an NMI is fired and initiate a shutdown. This prevent any loss off data or system crash. Before card insertion or removal, the flap switch is detected by FLP signal, a flap interrupt is fired which calls the card manager interrupt service routine, dealing with RAM, ROM or EPROM module.

![](./img/blink_sns.png)

The battery level monitoring circuit drives the SNS pin. On power failure (<3.2V), an NMI is fired and initiate a shutdown. 

##Slot 3 and Eprom programming
The connector for the top slot, slot 3, is moreover equipped to apply a programming voltage Vpp to enable an EPROM module to be programmed. By means of the present invention it is possible to effect this programming directly off the address and data buses extended to the module via the gate array and connector. Slot 3 normally operates like slots 1 and 2, i.e. normal read and (if RAM is in the slot) write operations may be effected. However a bit is used in the logic array as a flag to select (PROGRAM in COM register) between normal operation and programming mode. When this bit is set, the circuit described below with-reference to Fig. 2 is brought into action. A separate bit is set/reset to control the application of the programming voltage Vpp (VPP in COM register).

Features of the logic array are shown in Fig. 2. A particular address range, namely the top slot, is dedicated to EPROM programming, when the flag bit mentioned above is set. When the
CPU wishes to program an EPROM byte, it establishes the required address by way of the bus A0 to A15 and one of the registers 15 (B0 to B7) and the required data is put on to the data bus D0 to D7.

![](./img/blink_epr.png)

The logic array includes a decoder 22 which detects any extended write address in the programming range (B6 = B7 = 1) and issues a signal PROGRAMA which stops the CPU clock PHI CPU, via a latch 26 and gate 28. After a delay, introduced by delay stage 23, of about 2 seconds (FRONT PORCH) a latch 27 is set, which in turn will activate the appropriate control signals to the EPROM (such as PGM, OE and CE, depending on the EPROM type). Delay duration and state of PGM, EOE, SE3 are set in EPR register. When overprogramming mode (OVERP) is set in COM register, the delay set in EPR is increased 3 times.

The delay stage 24, is programmable, to accept various EPROM programming times. After the delay 24, the latch 27 is reset, thus releasing the EPROM from its programming mode. A delay 25 (BACK PORCH) lasts for about 2 seconds, after which the clock to the CPU is started again. Vpp is applied throughout this sequence of operations. The delay time of the delay 24 may be programmed to allow the CPU to perform a program then verify cycle repeatedly in a training stage during which the appropriate minimum delay for secure programming of the EPROM is ascertained.
It can thus be seen that the EPROM is programmed directly off the address and data buses A, D'0 - D'7 rather than off a separate register set up to buffer the input signals which have to be held over a very large number of CPU clock cycles.
 
##Keyboard
Fig. 3 is a block diagram of the keyboard 12 which has eight row conductors 1 to 8 and eight column conductors A to H. A simple key-switch (not shown) is provided at each of the sixty-four intersections and a depressed key connects the corresponding row and column conductors. The column conductors A to H are connected via diodes 30 and 2.2kohm resistors 31 to the address lines A8 to A15 respectively. The row conductors 1 to 8 are connected to +5V via 100 kohm pull-up resistors 32 and to the data lines Do to respectively via 47 kohm resistors 33. Keyboard sensing is by the well-known technique of sending a walking bit along the column conductors A to H and detecting which row conductor is pulled low.

![](./img/blink_keyb.png)

However the present invention makes it possible to do this with direct connection across the address and data buses, as will now be explained.

The diodes 30 and resistors 31 ensure that the CPU can always drive the address lines A8 to A15 as it wishes; they are not so loaded as to upset the drive thereof. On the other hand the large resistors 33 ensure-that the data lines D0 to D7 can normally be driven by the CPU or a device providing input to the CPU, even if some of the row conductors 1 to 8 are shorted together, because a plurality of keys are simultaneously depressed. When any of the address lines A8 to A15 goes low, it will tend to pull a data line low if any key is depressed. However, the relatively long time constant established by the resistors 33 will prevent any significant change in the voltage on the data line taking place over the time of a few clock cycles. Sensing the keyboard requires an interval of around three times the said time constant in order to pull the data line reliably low. This is effected by establishing the required state of the address lines A8 to A15 and then stopping the CPU clock for a few tens of cycles, around 20us (measure with a logic analyzer shows **exactly 30.4us**). The keyboard settling time is determined by a monostable in the logic array, similarly to what is described above in relation to Fig. 2. At the end of the settling interval, the clock is restarted and the CPU tests the data bus to ascertain which key was pressed.

When it is desired to wait for a key to be pressed, a software routine may be employed, in which all the address lines A8 to
A15 are driven low.

The following routine will loop until any key is pressed.

```
START:
	LD C,KEYPORT 		; I/O address of the keyboard port
	LD B,OO 			; Column data zero
LOOP:
	IN A,(C) 			; Read the keyboard
	CP $FF 			; Test for any rows low
	JP Z,LOOP 		; No keys pressed
```

The clock is stopped during the instruction IN A,(C) in order to freeze the address bus for the requisite number of cycles. In order to read which key has been pressed, on exit from the loop, it is necessary to read the keyboard using the instructions

```
LD B, 11111110
LD B, 11111101
LD B, 11111011 etc.
```

Another way to wait for a keypress is to stop the CPU clock indefinitely, with all zeroes on the address lines A8 to A15 and use an eight input NAND gate 34 to provide a signal ANYKEY to the logic array 11 in order to restart the CPU and perform whatever routine is appropriate, such as keyboard sensing to determine which key has been pressed.
